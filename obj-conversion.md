# OBJ Conversion Notes

Install the tools we need. Note that we need to install a special version of
`gltf-pipeline` because the Cesium one produces bad DRACO compressed outputs:

```shell
brew install imagemagick
npm install -g obj2gltf git+ssh://git@github.com/Avnerus/gltf-pipeline.git cwebp
```

## Mesh Normals
If the OBJ file doesn't have surface normals, then the Smithsonian Voyager
viewer will display a black surface. To add normals to your mesh:
1. Open the `.obj` file in MeshLab
2. Go to `File->Export...` or `File->Export As...`. The former option will
   **overwrite** the original mesh.
3. Make sure the `Normal` checkbox is checked in the `Vert` properties list.
3. Uncheck the `Save Texture File(s)` checkbox. If this is checked, it will
   overwrite the original texture image.
4. Click OK to save.

## Texture image size and format
The texture image must be `jpg` or `png` and have a max dimension of 8192.
Using ImageMagick, we can do both of these with one command:
```shell
convert -resize 8192x8192 input_image.tif output_img.png
```

Next, update the `.mtl` file to point to the new `.png` file:
```shell
nano input_mesh.mtl

# Find the map_Kd line and update the path
```

## Convert the mesh to a glb
First convert the `.obj` to a `.glb`, then compress with DRACO
compression:
```shell
# Convert
obj2gltf -i input_mesh.obj -o input_mesh.glb

# Compress
gltf-pipeline -i input_mesh.glb -o input_mesh_compressed.glb -d
```
