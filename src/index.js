// Imports, probably webpack specific paths
import * as THREE from 'three'
import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import { KTX2Loader } from 'three/examples/jsm/loaders/KTX2Loader'

// webpack specific: Get reference to local data
import pherc1186_cr05 from './data/PHerc1186Cr05_sm.glb'

// Setup scene and neutral background
const scene = new THREE.Scene()
scene.background = new THREE.Color(0xbfe3dd)

// Setup camera
const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.z = 5

// We don't want shadows on this mesh, so add an ambient light
const light = new THREE.AmbientLight(0xffffff)
scene.add(light)

// Setup the renderer and add to the DOM
const renderer = new THREE.WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
renderer.outputEncoding = THREE.sRGBEncoding
document.body.appendChild(renderer.domElement)

// Setup orbit controls, re-render when something changes
const controls = new OrbitControls(camera, renderer.domElement)
controls.addEventListener('change', render)
controls.minDistance = 0.5
controls.maxDistance = 10
controls.target.set(0, 0, 0)
controls.update()

// Add a window resize handler
window.addEventListener('resize', onWindowResize)

// Render once so we get the blue background while we load
render()

// Setup the GLTF loader
const loader = new GLTFLoader()

// Mesh is Draco compressed, so add it to the loader
const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('https://unpkg.com/three/examples/js/libs/draco/')
loader.setDRACOLoader(dracoLoader)

// Texture is KTX2 compressed, so add it to the loader
const ktx2Loader = new KTX2Loader()
ktx2Loader.setTranscoderPath('https://unpkg.com/three/examples/js/libs/basis/')
ktx2Loader.detectSupport(renderer)
loader.setKTX2Loader(ktx2Loader)

// Load the gltf file
const normal_mat = new THREE.MeshNormalMaterial()
normal_mat.side = THREE.DoubleSide
var texture_mat = null
loader.load(pherc1186_cr05, function (gltf) {
    // Give the mesh a name so we can find it later
    gltf.scene.children[0].name = 'model'
    texture_mat = gltf.scene.children[0].material
    texture_mat.side = THREE.DoubleSide
    // Add the loaded scene to the scene
    scene.add(gltf.scene)
    // Update the view
    render()
}, undefined, function (error) {
    console.error(error)
})

// Setup the GUI panel
const panel = new GUI({width:300})
var settings = {
    'wireframe': false,
    'normals': false,
}
panel.add(settings, 'wireframe').onChange(toggleWireframe).name('show wireframe')
panel.add(settings, 'normals').onChange(toggleNormals).name('show normals')

// Handle toggling wireframe
function toggleWireframe(toggled) {
    // Get our named model
    var model = scene.getObjectByName('model')
    // Set the material state
    model.material.wireframe = toggled
    render()
}

// Handle toggling texture
function toggleNormals(toggled) {
    // Get our named model
    var model = scene.getObjectByName('model')
    if(toggled) {
        model.material = normal_mat
    } else {
        model.material = texture_mat
    }
    model.material.wireframe = settings['wireframe']
    model.material.needsUpdate = true
    render()
}

// Handle window resizing
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    render()
}

// Handle render requests
function render() {
    renderer.render(scene, camera)
}
