const path = require('path');

 module.exports = {
   entry: './src/index.js',
   output: {
     filename: 'bundle.js',
     path: path.resolve(__dirname, 'public'),
   },
   module: {
     rules: [
       {
         test: /\.(glb|gltf)$/i,
         type: 'asset/resource',
       },
     ],
   },
 };
