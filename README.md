# 3D Viewer Example

## Build
```shell
npm install
npm run build
```

## Install web server
```shell
# Remove live-server (if you have it)
npm -g rm live-server
# Install five-server
npm install -g five-server
```

## Run web server
```shell
five-server public/
```
